package cr.ac.ucr.ecci.cql.examen_i_b25083;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;


public class DetailsFragment extends Fragment {

    public static ArrayList<String> tabletopName;
    public static ArrayList<String> tabletopDescription;
    public static ArrayList<Integer> tabletopIcon;
    private ArrayList<String> tabletopFullInfo;


    public static DetailsFragment newInstance(int index) {
        DetailsFragment f = new DetailsFragment();

        Bundle args = new Bundle();
        args.putInt("index", index);
        f.setArguments(args);
        return f;
    }

    public int getShownIndex() {
        return getArguments().getInt("index", 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        insertGame1();
        insertGame2();
        insertGame3();
        insertGame4();
        insertGame5();

        tabletopName = new ArrayList<>();
        tabletopIcon = new ArrayList<>();
        tabletopDescription = new ArrayList<>();
        tabletopFullInfo = new ArrayList<>();

        readTableTop("TT001");
        readTableTop("TT002");
        readTableTop("TT003");
        readTableTop("TT004");
        readTableTop("TT005");

        if(container == null) {
            return null;
        }

        ScrollView scroller = new ScrollView(getActivity());
        TextView text = new TextView(getActivity());
        int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getActivity().getResources().getDisplayMetrics());
        text.setPadding(padding, padding, padding, padding);
        scroller.addView(text);
        text.setText(tabletopFullInfo.get(getShownIndex()));
        return scroller;
    }

    // Se insertan los datos del juego 1
    private void insertGame1() {
        // Instancia la clase TableTop y realiza la inserción de datos
        TableTop tableTop = new TableTop(
                "TT001",
                "Catan",
                "1995",
                "Kosmos",
                "Germany",
                48.774538,
                9.188467,
                "Picture yourself in the era of discoveries:after a long voyage of great deprivation,your ships have finally reached the coast ofan uncharted island. Its name shall be Catan!But you are not the only discoverer. Otherfearless seafarers have also landed on theshores of Catan: the race to settle theisland has begun!",
                "3-4",
                "10+",
                "1-2 hours"
        );
        // inserta el juego, se le pasa como parametro el contexto de la app
        long newRowId = tableTop.insert(getActivity());
    }

    // Se insertan los datos del juego 2
    private void insertGame2() {
        // Instancia la clase TableTop y realiza la inserción de datos
        TableTop tableTop = new TableTop(
                "TT002",
                "Monopoly",
                "1935",
                "Hasbro",
                "United States",
                41.883736,
                -71.352259,
                "The thrill of bankrupting an opponent, but it pays to play nice, because fortunes could change with the roll of the dice. Experience the ups and downs by collecting property colors sets to build houses, and maybe even upgrading to a hotel!",
                "2-8",
                "8+",
                "20-180 minutes"
        );
        // inserta el juego, se le pasa como parametro el contexto de la app
        long newRowId = tableTop.insert(getActivity());
    }

    // Se insertan los datos del juego 3
    private void insertGame3() {
        // Instancia la clase TableTop y realiza la inserción de datos
        TableTop tableTop = new TableTop(
                "TT003",
                "Eldritch Horror",
                "2013",
                "Fantasy Flight Games",
                "United States",
                45.015417,
                -93.183995,
                "An ancient evil is stirring. You are part ofa team of unlikely heroes engaged in an international struggle to stop the gathering darkness. To do so, you’ll have to defeat foul monsters, travel to Other Worlds, andsolve obscure mysteries surrounding thisunspeakable horror.",
                "1-8",
                "14+",
                "2-4 hours"
        );
        // inserta el juego, se le pasa como parametro el contexto de la app
        long newRowId = tableTop.insert(getActivity());
    }

    // Se insertan los datos del juego 4
    private void insertGame4() {
        // Instancia la clase TableTop y realiza la inserción de datos
        TableTop tableTop = new TableTop(
                "TT004",
                "Magic: the Gathering",
                "1993",
                "Hasbro",
                "United States",
                41.883736,
                -71.352259,
                "Magic: The Gathering is a collectible and digital collectible card game created byRichard Garfield. Each game of Magic represents a battle between wizards known as planeswalkers who cast spells, use artifacts,and summon creatures.",
                "2+",
                "13+",
                "Varies"
        );
        // inserta el juego, se le pasa como parametro el contexto de la app
        long newRowId = tableTop.insert(getActivity());
    }

    // Se insertan los datos del juego 5
    private void insertGame5() {
        // Instancia la clase TableTop y realiza la inserción de datos
        TableTop tableTop = new TableTop(
                "TT005",
                "Hanabi",
                "2010",
                "Asmodee",
                "France",
                48.761629,
                2.065296,
                "Hanabi—named for the Japanese word for\"fireworks\"—is a cooperative game in whichplayers try to create the perfect fireworks show by placing the cards on the table in theright order.",
                "2-5",
                "8+",
                "25 minutes"
        );
        // inserta el juego, se le pasa como parametro el contexto de la app
        long newRowId = tableTop.insert(getActivity());
    }


    private void readTableTop(String id) {
        // Instancio el objeto
        TableTop tableTop = new TableTop();

        // Leo la fila
        tableTop.read(getActivity(), id);

        String fullInfo = "id: " + tableTop.getId() + " name: " + tableTop.getName() + " year: " + tableTop.getYear()
                + " publisher: " + tableTop.getPublisher() + " country: " + tableTop.getCountry() + " latitude: " + tableTop.getLatitude()
                + " longitude: " + tableTop.getLongitude() + " description " + tableTop.getDescription() + " numPlayers: " + tableTop.getNumPlayers()
                + " ages: " + tableTop.getAges() + " playingTime: " + tableTop.getPlayingTime();

        // LLeno la lista de juegos
        tabletopDescription.add(tableTop.getDescription());
        tabletopName.add(tableTop.getName());
        tabletopFullInfo.add(fullInfo);

        switch (id) {
            case "TT001":
                tabletopIcon.add(R.drawable.catan);
                break;
            case "TT002":
                tabletopIcon.add(R.drawable.monopoly);
                break;
            case "TT003":
                tabletopIcon.add(R.drawable.eldritch);
                break;
            case "TT004":
                tabletopIcon.add(R.drawable.mtg);
                break;
            case "TT005":
                tabletopIcon.add(R.drawable.hanabi);
                break;
        }
    }

}
