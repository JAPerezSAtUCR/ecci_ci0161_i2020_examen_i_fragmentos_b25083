package cr.ac.ucr.ecci.cql.examen_i_b25083;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class CustomListAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final ArrayList<String> itemname;
    private final ArrayList<Integer> imgid;
    private final ArrayList<String> itemdescription;
    public CustomListAdapter(Activity context, ArrayList<String> itemname, ArrayList<Integer> imgid, ArrayList<String>
            itemdescription) {
        super(context, R.layout.lista_personalizada, itemname);
        this.context = context;
        this.itemname = itemname;
        this.imgid = imgid;
        this.itemdescription = itemdescription;
    }
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.lista_personalizada, null, true);
        TextView name = (TextView) rowView.findViewById(R.id.name);
        ImageView image = (ImageView) rowView.findViewById(R.id.icon);
        TextView description = (TextView) rowView.findViewById(R.id.description);
        name.setText(itemname.get(position));
        image.setImageResource(imgid.get(position));
        description.setText(itemdescription.get(position));
        return rowView;
    }
}