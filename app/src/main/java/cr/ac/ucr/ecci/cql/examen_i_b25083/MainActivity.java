package cr.ac.ucr.ecci.cql.examen_i_b25083;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button buttonFragments = (Button) findViewById((R.id.buttonFragmentos));

        buttonFragments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToFragments();
            }
        });
    }

    private void goToFragments() {
        Intent intent = new Intent(this, FragmentsActivity.class);

        startActivity(intent);
    }



}
